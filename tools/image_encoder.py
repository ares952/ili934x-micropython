#!/usr/bin/python3

"""
Generate all icons in proper format. Filename of generated icon is printed to stdout.

Needs 2 parameters:
 * JSON file with defined icons and possible handling (resize)
 * Output dir, where are generatred icons stored

Json format:

{
    icon1,
    icon2,
    ...
    iconN
}

where each item is:

"path/to/image.png": {
    "o_name": "out",
    "resize": [ x, y ] (or) -1
}

 * o_name --- output name, .bin suffix added automaticaly
 * resize --- size of output image, if -1 unchanged.

All input images must be in indexed color format readable by PIL. Max colors count is 256 (pallete len must fit 1B).

Output image format is described in piclib

"""

from PIL import Image
import struct
import yaml
import sys
import getopt

def pix_per_byte(colors):
    """ Helper function,
        params:
          * colors: number of colors used in picture
        returns: tuple, which contains
          * number of pixels in 1B 
          * bit shift
          * bit mask for binary operations
    """

    # 1 pixel in 1B (>16 colors), no compression
    pix_per_byte = 1
    bite_shift = 0
    bite_mask = 255

    if colors < 17:
        # 2 pixels in 1B (4 < colors <= 16)
        pix_per_byte = 2
        bite_shift = 4
        bite_mask = 15
    if colors < 5:
        # 4 pixels in 1B (3 or 4 colors)
        pix_per_byte = 4
        bite_shift = 2
        bite_mask = 3
    if colors < 3:
        # 8 pixels in 1B (2 colors)
        pix_per_byte = 8
        bite_shift = 1
        bite_mask = 1

    return (pix_per_byte, bite_shift, bite_mask)


def colort565(color):
    """ color (tuple r,g,b) --> 16bit uint 5r6g5b
    """
    return (color[0] & 0xf8) << 8 | (color[1] & 0xfc) << 3 | color[2] >> 3


def colorit565(color):
    """ color 24bit (8r8b8g) uint --> 16bit uint 5r6g5b
    """
    return (color&0xf80000)>>8 | (color&0x00fc00)>>5 | (color&0x0000f8)>>3


def img_to_binary(image):
    """ Create bytearray from image
        Format:
            2B - X size of image
            2B - Y size of image
            1B - color pallete len
            2B - color no. 1 in 565 format
            ...
          N*2B - color no. N in 565 format (N == col. pal. len)
            4B - length of compressed image data
            NB - image data, N == length of compr. img data
    """

    #Header: X size, Y size, No. of colors
    data = list(struct.pack('>HHB', image.size[0], image.size[1], len(image.getcolors())))

    #Colors in 565
    for _, i in image.getcolors():
        data.extend(list(struct.pack('>H',
            colort565( (image.getpalette()[i*3],
                        image.getpalette()[i*3+1],
                        image.getpalette()[i*3+2])))))

    ppb, bs, bm = pix_per_byte(len(image.getcolors()))

    unc_data = list(image.getdata())
    nlen = len(unc_data)

    if len(unc_data) > (len(unc_data)//ppb)*ppb:
        # if compressed size will not fit whole bytes, append some zeroes to fit.
        nlen = (len(unc_data)//ppb+1)*ppb
        l = nlen - len(unc_data)
        for _ in range(l):
            unc_data.append(0)

    #Len of data
    data.extend(list(struct.pack('>L', nlen//ppb)))

    # ...aaand data
    for i in range(nlen//ppb):
        c_data = 0
        for j in range(ppb):
            c_data = (c_data<<bs) + unc_data[i*ppb+j]
        data.append(c_data)

    return bytearray(data)


### Main

if __name__ == '__main__':

    idx = 1
    if 'python3' in sys.argv[0]:
        idx = 2

    try:
        opts, args = getopt.getopt(sys.argv[idx:], "hc:d:", ["help", "config=", "dest="])
    except getopt.GetoptError:
        print ('image_encoder.py -c <config.yml> -d <out_dir>')
        sys.exit(2)

    for opt, arg in opts:
        if opt == ('-h', '--help'):
            print('image_encoder.py -c <config.yml> -d <out_dir>')
            sys.exit()
        elif opt in ("-c", "--config"):
            json_name = arg
        elif opt in ("-d", "--dest"):
            export_dir = arg

    fj = open(json_name)
    ikony = yaml.safe_load(fj)
    fj.close()

    for ikona in ikony['icons']:

        f_bin = open("{}/{}.bin".format(export_dir, ikona['icon']), 'wb')

        ik = Image.open(ikona['file'])

        try:
            ik.getcolors()
        except AttributeError as e:
            ik.getcolors()

        if ikona['resize'] != -1:
            res = tuple(ikona['resize'])
            try:
                ik = ik.resize(res)
            except AttributeError as e:
                ik = ik.resize(res)

        binimg = img_to_binary(ik)

        f_bin.write(binimg)
        f_bin.close()

        print("{}/{}.bin".format(export_dir, ikona['icon']))
