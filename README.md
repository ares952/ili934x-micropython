# ILI934x-micropython

Library for  using ILI9341 display drivers with Micropython. Based on 
  * [micropython-ili9341](https://github.com/jeffmer/micropython-ili9341.git) -- core of library and basic functions
  * [Loboris Micropython port](https://github.com/loboris/MicroPython_ESP32_psRAM_LoBo) -- font format and tool for creating C font files (used in original Loboris MP port)

This library have restricted capabilities, because I do not need draw thing like triangles, circles, etc. You can draw text (with own font), draw images, rectangular, set specified color to specified pixel...

## Usage

### Example

## Images format

Source images must be with indexed color schema, in required size (no scaling)

### Tools for image creation



## Font format

Font drawing is based on Loboris work -- use his font format translated to PY lib

Great thing is, you can have only chars, which you want to use. Unnecessary characters can be removed to save memory.

Part of this repository is base font - rcr12.py (Roboto Condensed Regular, 12px)

### Format description

font itself is organised as bytearray, where first 4B are header and rest is description of each character included:

| header | 4B  |
|--------|-----|
| char1  | nB  |
| char2  | nB  |
| ...    | ... |
| charN  | nB  |

Header bytes:

| 1    | 2          | 3    | 4    |
|------|------------|------|------|
| 0x00 | font heigh | 0x00 | 0x00 |

And each character have format:

| char       | y\_offset                 | width                     | height                     | x\_offset                 | x\_delta                     | data...                 |
|------------|---------------------------|---------------------------|----------------------------|---------------------------|------------------------------|-------------------------|
| 1B, ord(c) | 1B, Offset in Y dimension | 1B, width of visible area | 1B, height of visible area | 1B, Offset in X dimension | 1B, Effective height of char | nB, encoded char,       |
| 'A' = 0x41 |                           |                           |                            |                           |                              | ((w\*h)\/8) + 1 if rest |


```
q_char = b'q\x06\x06\n\x02\x0a=\x14QE\x13\xc1\x04\x10'
```

```
char: q
   w: 06,    h:10
xoff: 02, yoff:06
xdel: 10, fnth:12

+------x delta-----+
+xoff+---width---+ |
|    |           | |
 0 1 2 3 4 5 6 7 8 9
 . . | . . . . . | . |00--+--+
 . . | . . . . . | . |01  Y  |
 . . | . . . . . | . |02  o  f
 . . | . . . . . | . |03  f  o
 . . | . . . . . | . |04  f  n
 . . | . . . . . | . |05  |  t
-----+--########-+-- |06--+  h
 . . |## . . .## | . |07  |  e
 . . |## . . .## | . |08  h  i
 . . |## . . .## | . |09  e  g
 . . |## . . .## | . |10  i  h
 . . |## . . .## | . |11  g  t
_____|__########_|__ |12- h -+
 . . | . . . .## | . |13  t
 . . | . . . .## | . |14  |
 . . | . . . .## | . |15--+

```

Header and all characters are in continous bytearray, terminated with 0xFF on position of last char.

Library `font.py` expects existing variable mvfont, which is memoryview containing described bytearray.


### Tools for font creation

First tool is ''ttf2c_vc2003.exe'', which convert TTF file to C source file.

This is a command line Windows program, but can be used under Linux with wine:

Usage:

```
ttf2c_vc2003.exe <point-size> <input-file> <output-file> [<start_char> <end_char>]
```

or, under Linux:

```
wine ./ttf2c_vc2003.exe <point-size> <input-file> <output-file> [<start_char> <end_char>]
```

Example:

```
wine ./ttf2c_vc2003.exe 18 Vera.ttf vera.c
```

After the c source is created, use second tool: `c2py_font_gen.py`. This one generates python source code, which can be used directly in ili934x library.

Usage:

```
c2py_font_gen.py -i <input-C-file> -o <output-PY--file>
```




## Limitations

 * Not deeply tested, maybe bugs inside
 * Drawing text is slow.
 * Only 16bit color formats


